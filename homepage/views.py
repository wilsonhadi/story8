from django.shortcuts import render, redirect

# Create your views here.

def index(request):
    response = {}
    return render(request, 'index.html', response)